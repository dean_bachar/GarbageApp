import express from "express";
import asyncHandler from "express-async-handler";

let isAliveRouter = express.Router();

isAliveRouter.get(
  "/",
  asyncHandler(async (req, res) => {
    res.status(200).send("ok");
  })
);

export default isAliveRouter;