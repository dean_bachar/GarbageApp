import mongoose from "mongoose";
import { DB_CONNECTION } from "./envs.js";


const connectDbs = async () => {
  try {
    await mongoose.connect(DB_CONNECTION, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    });
    console.log("Connection to mongo succeeded");
  } catch (err) {
    console.error(`Failed to connect to MongoDB on ${DB_CONNECTION}`, err);
    throw err;
  }
};

export default connectDbs;
