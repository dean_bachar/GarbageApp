import express from "express";
import garbagesRouter from "./routers/garbages.js";
import isAliveRouter from "./routers/isAlive.js";
import logger from "./middlewares/logger.js";
import errorHandler from "./middlewares/errorHandler.js";
import connectDbs from "./dbsConnect.js";
import allowAccessControlOrigin from "./middlewares/accessControl.js";
import { PORT } from "./envs.js"

connectDbs();
export const app = express();
app.use(express.json());

app.use(logger);

if (app.get("env") === "development") app.use(allowAccessControlOrigin);

app.use("/api/garbages", garbagesRouter);
app.use("/api/isAlive", isAliveRouter);

app.use(errorHandler);

app.listen(PORT, () => {
  console.log(`Starting the garbages app on port ${PORT}`);
});
